import javax.swing.*;
import java.awt.Color;

public class TrafficLights {

    final Color darkGreen = new Color(0x193300);
    final Color brightGreen = new Color(0x00FF00);
    final Color darkAmber = new Color(0x666600);
    final Color brightAmber = new Color(0xFFFF33);
    final Color darkRed = new Color(0x330000);
    final Color brightRed = new Color(0xFF0000);
    private JPanel TrafficLightsPanel;
    private JButton redButton;
    private JButton amberButton;
    private JButton greenButton;
    private TrafficLightSequencer trafficLights;
    private Light greenLight;
    private Light amberLight;
    private Light redLight;
    private AsynchronousOneShotTimer timer;

    public TrafficLights() {
        greenLight = new UILight(greenButton, brightGreen, darkGreen);
        amberLight = new UILight(amberButton, brightAmber, darkAmber);
        redLight = new UILight(redButton, brightRed, darkRed);
        timer = new AsynchronousOneShotTimer();

        trafficLights = new TrafficLightSequencer(redLight, amberLight, greenLight, timer);
        trafficLights.Run();
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("TrafficLights");
        frame.setContentPane(new TrafficLights().TrafficLightsPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
