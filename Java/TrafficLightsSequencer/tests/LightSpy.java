public class LightSpy implements Light
{
    private boolean lit;

    LightSpy()
    {
        lit = false;
    }

    public void SwitchOn()
    {
        lit = true;
    }

    public void SwitchOff()
    {
        lit = false;
    }

    public boolean IsLit()
    {
        return lit;
    }
}
