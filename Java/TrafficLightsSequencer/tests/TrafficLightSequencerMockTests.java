import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;

import static org.mockito.Mockito.*;

public class TrafficLightSequencerMockTests {

    final int TWO_SECONDS = 2;
    final int TWO_MINUTES = 120;
    final int TWO_MINUTES_TWO_SECOND = 122;
    final int FOUR_MINUTES_AND_TWO_SECOND = 242;
    final int FOUR_MINUTES_AND_FOUR_SECONDS = 244;
    private Light redLight;
    private Light amberLight;
    private Light greenLight;
    private OneShotTimer oneShotTimer;
    private TrafficLightSequencer trafficLights;

    @Before
    public void setUp() throws Exception {

        redLight = mock(Light.class);
        greenLight = mock(Light.class);
        amberLight = mock(Light.class);
        oneShotTimer = mock(OneShotTimer.class);
        trafficLights = new TrafficLightSequencer(redLight, amberLight, greenLight, oneShotTimer);
    }

    private void VerifyScheduledForDurationAndInvokeCallback(int durationInSeconds)
    {
        ArgumentCaptor<Runnable> captor = ArgumentCaptor.forClass(Runnable.class);

        verify(oneShotTimer, atLeastOnce()).Schedule(eq(durationInSeconds), captor.capture());

        captor.getValue().run();
    }

    private void AssertRedLightIsLit()
    {
        verify(redLight, LastCall.lastCall()).SwitchOn();
        verify(amberLight, LastCall.lastCall()).SwitchOff();
        verify(greenLight, LastCall.lastCall()).SwitchOff();
    }

    private void AssertAmberLightIsLit()
    {
        verify(redLight, LastCall.lastCall()).SwitchOff();
        verify(amberLight, LastCall.lastCall()).SwitchOn();
        verify(greenLight, LastCall.lastCall()).SwitchOff();
    }

    private void AssertGreenLightIsLit()
    {
        verify(redLight, LastCall.lastCall()).SwitchOff();
        verify(amberLight, LastCall.lastCall()).SwitchOff();
        verify(greenLight, LastCall.lastCall()).SwitchOn();
    }

    private void AssertRedAndAmberLightsAreLit()
    {
        verify(redLight, LastCall.lastCall()).SwitchOn();
        verify(amberLight, LastCall.lastCall()).SwitchOn();
        verify(greenLight, LastCall.lastCall()).SwitchOff();
    }

    // This test is superceded by Run_ZeroSecondsElapsed_Stop but has been left to demonstrate
    // how the tests evolved incrementally. Normally this should now be deleted.
    @Test
    public void Run_ZeroSecondsElapsed_RedLightIsLit() throws Exception
    {
        trafficLights.Run();

        verify(redLight, only()).SwitchOn();
    }

    // This test is superceded by Run_ZeroSecondsElapsed_Stop but has been left to demonstrate
    // how the tests evolved incrementally. Normally this should now be deleted.
    @Test
    public void Run_ZeroSecondsElapsed_AmberLightIsNotLit() throws Exception
    {
        trafficLights.Run();

        verify(amberLight, only()).SwitchOff();
    }

    // This test is superceded by Run_ZeroSecondsElapsed_Stop but has been left to demonstrate
    // how the tests evolved incrementally. Normally this should now be deleted.
    @Test
    public void Run_ZeroSecondsElapsed_GreenLightIsNotLit() throws Exception {

        trafficLights.Run();

        verify(greenLight, only()).SwitchOff();
    }

    @Test
    public void Run_ZeroSecondsElapsed_Stop() throws Exception
    {
        trafficLights.Run();

        AssertRedLightIsLit();
    }

    @Test
    public void Run_TwoMinutesElapsed_PrepareToGo() throws Exception
    {
        trafficLights.Run();

        VerifyScheduledForDurationAndInvokeCallback(TWO_MINUTES);

        AssertRedAndAmberLightsAreLit();
    }

    @Test
    public void Run_TwoMinutesTwoSecondsElapsed_Go() throws Exception
    {
        trafficLights.Run();

        VerifyScheduledForDurationAndInvokeCallback(TWO_MINUTES);
        VerifyScheduledForDurationAndInvokeCallback(TWO_SECONDS);

        AssertGreenLightIsLit();
    }

    @Test
    public void Run_FourMinutesTwoSecondsElapsed_PrepareToStop() throws Exception
    {
        trafficLights.Run();

        VerifyScheduledForDurationAndInvokeCallback(TWO_MINUTES);
        VerifyScheduledForDurationAndInvokeCallback(TWO_SECONDS);
        VerifyScheduledForDurationAndInvokeCallback(TWO_MINUTES);

        AssertAmberLightIsLit();
    }

    @Test
    public void Run_FourMinutesFourSecondsElapsed_Stop() throws Exception
    {
        trafficLights.Run();

        VerifyScheduledForDurationAndInvokeCallback(TWO_MINUTES);
        VerifyScheduledForDurationAndInvokeCallback(TWO_SECONDS);
        VerifyScheduledForDurationAndInvokeCallback(TWO_MINUTES);
        VerifyScheduledForDurationAndInvokeCallback(TWO_SECONDS);

        AssertRedLightIsLit();
    }
}
