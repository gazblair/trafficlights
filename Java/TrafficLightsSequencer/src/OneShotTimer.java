public interface OneShotTimer
{
    void Schedule(int timeoutInSeconds, Runnable callbackFunction);
}
