public class TrafficLightSequencer {
    public static final int SECOND_DURATION_UNTIL_NEXT_SEQUENCE_START = 120;
    public static final int INTER_SEQUENCE_SECOND_DURATION = 2;
    private Light redLight;
    private Light amberLight;
    private Light greenLight;
    private OneShotTimer oneShotTimer;

    TrafficLightSequencer(Light newRedLight,
                          Light newAmberLight,
                          Light newGreenLight,
                          OneShotTimer newOneShotTimer)
    {
        redLight = newRedLight;
        amberLight = newAmberLight;
        greenLight = newGreenLight;
        oneShotTimer = newOneShotTimer;
    }
    public void Run()
    {
        Stop();
    }

    private void Stop()
    {
        amberLight.SwitchOff();
        greenLight.SwitchOff();
        redLight.SwitchOn();

        oneShotTimer.Schedule(SECOND_DURATION_UNTIL_NEXT_SEQUENCE_START, this::PrepareToGo);
    }

    public void PrepareToGo()
    {
        amberLight.SwitchOn();

        oneShotTimer.Schedule(INTER_SEQUENCE_SECOND_DURATION, this::Go);
    }

    public void Go()
    {
        redLight.SwitchOff();
        amberLight.SwitchOff();
        greenLight.SwitchOn();

        oneShotTimer.Schedule(SECOND_DURATION_UNTIL_NEXT_SEQUENCE_START, this::PrepareToStop);
    }

    public void PrepareToStop()
    {
        greenLight.SwitchOff();
        amberLight.SwitchOn();

        oneShotTimer.Schedule(INTER_SEQUENCE_SECOND_DURATION, this::Stop);
    }
}
