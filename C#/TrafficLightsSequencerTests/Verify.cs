﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using TrafficLightsSequencer;

namespace TrafficLightsSequencerTests
{
    public class Verify
    {
        private LastCall redLightLastCall;
        private LastCall amberLightLastCall;
        private LastCall greenLightLastCall;

        public Verify(Mock<Light> redLight,
            Mock<Light> amberLight,
            Mock<Light> greenLight)
        {
            redLightLastCall = new LastCall(redLight);
            amberLightLastCall = new LastCall(amberLight);
            greenLightLastCall = new LastCall(greenLight);
        }

        public void StopState()
        {
            redLightLastCall.VerifySwitchedOn();
            amberLightLastCall.VerifySwitchedOff();
            greenLightLastCall.VerifySwitchedOff();
        }

        public void PrepareToGoState()
        {
            redLightLastCall.VerifySwitchedOn();
            amberLightLastCall.VerifySwitchedOn();
            greenLightLastCall.VerifySwitchedOff();
        }

        public void GoState()
        {
            redLightLastCall.VerifySwitchedOff();
            amberLightLastCall.VerifySwitchedOff();
            greenLightLastCall.VerifySwitchedOn();
        }

        public void PrepareToStopState()
        {
            redLightLastCall.VerifySwitchedOff();
            amberLightLastCall.VerifySwitchedOn();
            greenLightLastCall.VerifySwitchedOff();
        }
    }
}
