﻿using Moq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TrafficLightsSequencer;
using System.Collections.Generic;

namespace TrafficLightsSequencerTests
{
    public class TimeoutInvoker
    {
        private Mock<OneShotTimer> oneShotTimer;
        private int pendingInvokations;
        private List<int> timeouts;

        public TimeoutInvoker(Mock<OneShotTimer> newOneShotTimer)
        {
            oneShotTimer = newOneShotTimer;
            timeouts = new List<int>();
            InvokeNumberOfTimes(0);
        }

        public int Count { get { return timeouts.Count; } }

        public int Occurrence(int occurrence)
        {
            return timeouts[occurrence - 1];
        }

        public int LastOccurrence { get { return timeouts[timeouts.Count - 1]; } }

        public void InvokeNumberOfTimes(int numberOfTimeouts)
        {
            pendingInvokations = numberOfTimeouts;

            oneShotTimer.Setup(m => m.Schedule(It.IsAny<int>(), It.IsAny<TimeoutReached>())).Callback(
                (int timeout, TimeoutReached timeoutReached) =>
                {
                    timeouts.Add(timeout);

                    if (pendingInvokations > 0)
                    {
                        pendingInvokations--;
                        timeoutReached();
                    }

                });
        }
    }
}
