﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrafficLightsSequencer
{
    public delegate void TimeoutReached();

    public interface OneShotTimer
    {
        void Schedule(int timeoutInSeconds, TimeoutReached timeoutReached);
    }
}
