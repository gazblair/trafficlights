﻿using TrafficLightsSequencer;
using System.Timers;

namespace TrafficLights
{
    class AsynchronousOneShotTimer : OneShotTimer
    {
        private TimeoutReached timeoutReached;
        private Timer timer = null;

        private void OnTimedEvent(object source, System.Timers.ElapsedEventArgs e)
        {
            timeoutReached();
        }

        public void Schedule(int timeoutInSeconds, TimeoutReached newTimeoutReached)
        {
            StopCurrentlyRunningTimerIfNecessary();
            timeoutReached = newTimeoutReached;
            CreateNewTimer(timeoutInSeconds);
        }

        private void CreateNewTimer(int timeoutInSeconds)
        {
            timer = new Timer(timeoutInSeconds * 1000);
            timer.Elapsed += OnTimedEvent;
            timer.AutoReset = false;
            timer.Start();
        }

        private void StopCurrentlyRunningTimerIfNecessary()
        {
            if (timer != null)
            {
                timer.Stop();
            }
        }
    }
}

