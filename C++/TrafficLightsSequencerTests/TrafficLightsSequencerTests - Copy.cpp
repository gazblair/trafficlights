#include "TrafficLightsSequencer.h"
#include "LightMock.h"
#include "OneShotTimerMock.h"

#include "Light.h"

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <string>
#include <vector>

using ::testing::Expectation;
using ::testing::AtLeast;
using ::testing::AtMost;
using ::testing::InvokeArgument;
using ::testing::Assign;
using ::testing::Invoke;
using ::testing::_;
using ::testing::InSequence;

class LightSetting {
public:
    bool isLit;
    bool IsLit() { return isLit; }
    void SetIsLit() { isLit = true; }
    void SetIsNotLit() { isLit = false; }
};
class TrafficLightsSequencerTest : public ::testing::Test {

public:
    static const int TwoMinutes = 120;
    static const int TwoSeconds = 2;

protected:
    LightMock redLightMock;
    LightMock amberLightMock;
    LightMock greenLightMock;
    LightSetting redSetting;
    LightSetting amberSetting;
    LightSetting greenSetting;
    OneShotTimerMock oneShotTimerMock;
    TrafficLightsSequencer sequencer;
    bool lastRedLightCallSwitchedOff;
    bool lastAmberLightCallSwitchedOff;
    bool lastGreenLightCallSwitchedOff;

    TrafficLightsSequencerTest() : sequencer(redLightMock, amberLightMock, greenLightMock, oneShotTimerMock) 
    {
        ON_CALL(redLightMock, SwitchOff()).WillByDefault(Invoke(&redLightMock, &LightMock::RegisterSwitchOff));
        ON_CALL(redLightMock, SwitchOn()).WillByDefault(Invoke(&redLightMock, &LightMock::RegisterSwitchOn));
    }

    void ExpectLightsToBeInStopState()
    {
        ExpectLightWasLastSwitchedOn(redLightMock, lastRedLightCallSwitchedOff);
        ExpectLightWasLastSwitchedOff(amberLightMock, lastAmberLightCallSwitchedOff);
        ExpectLightWasLastSwitchedOff(greenLightMock, lastGreenLightCallSwitchedOff);
    }

    void ExpectLightsToBeInPrepareToGoState()
    {
        ExpectLightWasLastSwitchedOn(redLightMock, lastRedLightCallSwitchedOff);
        ExpectLightWasLastSwitchedOn(amberLightMock, lastAmberLightCallSwitchedOff);
        ExpectLightWasLastSwitchedOff(greenLightMock, lastGreenLightCallSwitchedOff);
    }

    void ExpectLightsToBeInGoState()
    {
        ExpectLightWasLastSwitchedOff(redLightMock, lastRedLightCallSwitchedOff);
        ExpectLightWasLastSwitchedOff(amberLightMock, lastAmberLightCallSwitchedOff);
        ExpectLightWasLastSwitchedOn(greenLightMock, lastGreenLightCallSwitchedOff);
    }

    void ExpectLightWasLastSwitchedOff(LightMock& light, bool lastLightCallSwitchedOff)
    {
        
        //LightSetting local;
     //   ON_CALL(light, SwitchOn()).WillByDefault(Assign(&lastLightCallSwitchedOff, true));

        //    Assign(&variable, value)
//        {
//            InSequence s;
//
//            Expectation a = EXPECT_CALL(light, SwitchOn()).Times(AtLeast(1));
//            //EXPECT_CALL(light, SwitchOff()).Times(AtMost(0));
//            EXPECT_CALL(light, Die()).Times(1).After(a);
//
////            EXPECT_CALL(light, SwitchOn()).Times(AtLeast(0));
//            EXPECT_CALL(light, SwitchOff()).Times(AtLeast(0));
//            EXPECT_CALL(light, SwitchOff()).Times(AtLeast(1));
//            //EXPECT_CALL(light, SwitchOn()).Times(AtMost(0));
//            EXPECT_CALL(light, Die()).Times(1);
//        }

        //Expectation a = EXPECT_CALL(light, SwitchOff()).Times(AtLeast(0));

        //Expectation b = EXPECT_CALL(light, SwitchOn()).Times(AtLeast(1)).After(a);
        //EXPECT_CALL(light, SwitchOff()).Times(AtMost(0)).After(b);
        //EXPECT_CALL(light, Die()).Times(1).After(b,a);

   }

    void ExpectLightWasLastSwitchedOn(LightMock& light, bool lastLightCallSwitchedOff)
    {
        //ON_CALL(light, SwitchOn()).WillByDefault(Invoke((&redSetting, &LightSetting::SetIsLit)));

        //ON_CALL(light, SwitchOn()).WillByDefault(Assign(&lastLightCallSwitchedOff, false));
 
//        {
//            InSequence s;
//
//            //EXPECT_CALL(light, SwitchOff()).Times(AtLeast(0));
//            //EXPECT_CALL(light, SwitchOff()).Times(AtMost(0));
////            EXPECT_CALL(light, Die()).Times(1);
//
//        }

        //Expectation a = EXPECT_CALL(light, SwitchOn()).Times(AtLeast(1));
        //EXPECT_CALL(light, Die()).Times(1).After(a);

        //Expectation a = EXPECT_CALL(light, SwitchOn()).Times(AtLeast(0));

        //Expectation b = EXPECT_CALL(light, SwitchOff()).Times(AtLeast(1)).After(a);
        //EXPECT_CALL(light, SwitchOn()).Times(AtMost(0)).After(b);


        //Expectation init_x = EXPECT_CALL(light, SwitchOff())
        //    .Times(AtLeast(0));

        //EXPECT_CALL(light, SwitchOn()).Times(AtLeast(1)).After(init_x);
    }

    void ExpectTimerScheduled(int timeoutInSeconds)
    {
        EXPECT_CALL(oneShotTimerMock, Schedule(timeoutInSeconds, _));
    }

    void ExpectTimerScheduledAndInvokeCallback(int timeoutInSeconds)
    {
        //ON_CALL(oneShotTimerMock, Schedule(timeoutInSeconds, _)).WillByDefault(InvokeArgument<1>());

        EXPECT_CALL(oneShotTimerMock, Schedule(timeoutInSeconds, _))
            .WillOnce(InvokeArgument<1>());
    }

};


TEST_F(TrafficLightsSequencerTest, Run_ZeroSecondsElapsed_RedLightIsLit) {

    EXPECT_CALL(redLightMock, SwitchOn())
        .Times(1);

    sequencer.Run();
}

TEST_F(TrafficLightsSequencerTest, Run_ZeroSecondsElapsed_AmberLightIsNotLit) {
    EXPECT_CALL(amberLightMock, SwitchOff())
        .Times(1);

    sequencer.Run();
}

TEST_F(TrafficLightsSequencerTest, Run_ZeroSecondsElapsed_GreenLightIsNotLit) {
    EXPECT_CALL(greenLightMock, SwitchOff())
        .Times(1);

    sequencer.Run();
}


TEST_F(TrafficLightsSequencerTest, Run_ZeroSecondsElapsed_Stop) {
    ExpectLightsToBeInStopState();

    sequencer.Run();
}

TEST_F(TrafficLightsSequencerTest, Run_ZeroSecondsElapsed_SchedulePrepareToGo) {
    
    ExpectTimerScheduled(TwoMinutes);

    sequencer.Run();
}

TEST_F(TrafficLightsSequencerTest, Run_TwoMinutesElapsed_PrepareToGo) {
    ExpectLightsToBeInPrepareToGoState();

    ExpectTimerScheduledAndInvokeCallback(TwoMinutes);
    ExpectTimerScheduled(TwoSeconds);
    sequencer.Run();
}

TEST_F(TrafficLightsSequencerTest, Run_TwoMinutesTwoSecondsElapsed_Go) {
    
    //ExpectLightWasLastSwitchedOn(greenLightMock);
    //ExpectLightWasLastSwitchedOff(greenLightMock);
    //ExpectLightWasLastSwitchedOff(redLightMock);
    EXPECT_CALL(oneShotTimerMock, Schedule(_, _)).WillRepeatedly(InvokeArgument<1>());
        //.WillOnce(InvokeArgument<1>()).WillOnce(InvokeArgument<1>()).WillOnce(InvokeArgument<1>());

//    ExpectLightsToBeInGoState();

    //ExpectTimerScheduledAndInvokeCallback(TwoMinutes);
    //ExpectTimerScheduledAndInvokeCallback(TwoSeconds);
    //ExpectTimerScheduled(TwoMinutes);

    sequencer.Run();

    ASSERT_TRUE(redLightMock.IsLastCallSwitchOff());
}

int main(int argc, char *argv[]) {
    ::testing::InitGoogleMock(&argc, argv);
    return RUN_ALL_TESTS();
}
