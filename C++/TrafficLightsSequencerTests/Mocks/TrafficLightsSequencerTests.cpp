#include "TrafficLightsSequencerTestFixture.h"

#include "TrafficLightsSequencer.h"

#include <gtest/gtest.h>

TEST_F(TrafficLightsSequencerTestFixture, Run_ZeroSecondsElapsed_RedLightIsLit) {

    EXPECT_CALL(redLightMock, SwitchOn())
        .Times(1);

    sequencer.Run();
}

TEST_F(TrafficLightsSequencerTestFixture, Run_ZeroSecondsElapsed_AmberLightIsNotLit) {
    EXPECT_CALL(amberLightMock, SwitchOff())
        .Times(1);

    sequencer.Run();
}

TEST_F(TrafficLightsSequencerTestFixture, Run_ZeroSecondsElapsed_GreenLightIsNotLit) {
    EXPECT_CALL(greenLightMock, SwitchOff())
        .Times(1);

    sequencer.Run();
}

TEST_F(TrafficLightsSequencerTestFixture, Run_ZeroSecondsElapsed_Stop) {
    sequencer.Run();

    AssertLightsInStopState();
}

TEST_F(TrafficLightsSequencerTestFixture, Run_ZeroSecondsElapsed_SchedulePrepareToGo) {
    
    ExpectTimerScheduled(TwoMinutes);

    sequencer.Run();
}

TEST_F(TrafficLightsSequencerTestFixture, Run_TwoMinutesElapsed_PrepareToGo) {

    ExpectListOfScheduledTimeoutsAndTriggerInvoke({ TwoMinutes, TwoSeconds});

    sequencer.Run();

    AssertLightsInPrepareToGoState();
}

TEST_F(TrafficLightsSequencerTestFixture, Run_TwoMinutesTwoSecondsElapsed_Go) {

    ExpectListOfScheduledTimeoutsAndTriggerInvoke({ TwoMinutes, TwoSeconds, TwoMinutes });

    sequencer.Run();

    AssertLightsInGoState();
}

TEST_F(TrafficLightsSequencerTestFixture, Run_FourMinutesTwoSecondsElapsed_PrepareToStop) {

    ExpectListOfScheduledTimeoutsAndTriggerInvoke({ TwoMinutes, TwoSeconds, TwoMinutes, TwoSeconds });

    sequencer.Run();

    AssertLightsInPrepareToStopState();
}

TEST_F(TrafficLightsSequencerTestFixture, Run_FourMinutesFourSecondsElapsed_Stop) {

    ExpectListOfScheduledTimeoutsAndTriggerInvoke({ TwoMinutes, TwoSeconds, TwoMinutes, TwoSeconds, TwoMinutes });

    sequencer.Run();

    AssertLightsInStopState();
}

int main(int argc, char *argv[]) {
    ::testing::InitGoogleMock(&argc, argv);
    return RUN_ALL_TESTS();
}
