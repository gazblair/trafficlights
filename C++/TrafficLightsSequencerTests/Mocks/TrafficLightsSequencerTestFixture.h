#pragma once

#include "TrafficLightsSequencer.h"
#include "OneShotTimerMock.h"

#include "LightMock.h"

#include <gtest/gtest.h>

#include <vector>

class TrafficLightsSequencerTestFixture : public ::testing::Test {

public:
    static const int TwoMinutes = 120;
    static const int TwoSeconds = 2;

protected:
    LightMock redLightMock;
    LightMock amberLightMock;
    LightMock greenLightMock;
    OneShotTimerMock oneShotTimerMock;
    TrafficLightsSequencer sequencer;

    TrafficLightsSequencerTestFixture();

    void AssertLightsInStopState();
    void AssertLightsInPrepareToGoState();
    void AssertLightsInGoState();
    void AssertLightsInPrepareToStopState();
    void ExpectTimerScheduled(int timeoutInSeconds);
    void ExpectTimerScheduledAndInvokeCallback(int timeoutInSeconds);
    void ExpectListOfScheduledTimeoutsAndTriggerInvoke(std::vector<int> scheduledTimeouts);
};

